# HFL: Hybrid Fuzzing on the Linux Kernel 

An open-source hybrid fuzzing framework for the Linux kernel.



# Requirements
The following setup instructions have been tested on Ubuntu 14.04.

### Go 

[Go](https://golang.org/dl/) toolchain is required for build. 
Download and then edit `$HFLHOME/scripts/common.sh` 
(`$GOROOT` to point to `/path/to/go`)

### Download linux kernel source package

```
$ cd $HFLHOME/scripts/
$ ./download_kernel.sh
```

# Setup
The following is an introduction on how to setup HFL for Linux kernel fuzzing.


* Set necessary environment variables:
```
$ cd $HFLHOME/scripts/
$ source ./common.sh
```

## GCC
HFL's static analysis component is implemented on GCC compiler. 

* Build GCC:
```
$ ./build-gcc.sh
```

## S2E
HFL's symbolic executor is based on S2E.


* Build S2E:
```
$ cd $HFLHOME/scripts/s2e/
$ ./build-s2e.sh
```

* Build the kernel for S2E:
```
$ ./build-kernel.sh -conf [config] 
```

* Create disk image for S2E:
```
$ ./build-image.sh 
```

* Take snapshots:
```
$ ./snapshots.sh
```

* Edit `$HFLHOME/scripts/s2e-config.lua` 
(`baseDirs` in `pluginsConfig.HostFiles` to point to `/path/to/HFL/scripts/tmp`)


## Syzkaller
A fuzzing component of HFL is implemented upon Syzkaller.


* Build Syzkaller:
```
$ cd $HFLHOME/scripts/syz/
$ ./build-syzkaller.sh 
```

* Create disk image for Syzkaller:
```
$ ./build-image.sh 
```
* Copy the public key `$HFLHOME/scripts/id_rsa.pub` into the created image

* Build the kernel for Syzkaller:
```
$ ./build-kernel.sh -conf [config] 
```


# Run HFL

```
$ cd $HFLHOME/scripts/
$ ./run_syzkaller.sh
$ ./run_s2e.sh
$ ./run_agent.sh
```
