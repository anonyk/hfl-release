// Copyright 2015 syzkaller project authors. All rights reserved.
// Use of this source code is governed by Apache 2 LICENSE that can be found in the LICENSE file.

package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
	"math/rand"
	"encoding/binary"

	"github.com/google/syzkaller/prog"
	_ "github.com/google/syzkaller/sys"
)

var (
	flagOS         = flag.String("os", "linux", "target os")
	flagArch       = flag.String("arch", "amd64", "target arch")
	flagProg       = flag.String("prog", "", "file with program to convert (required)")
	flagSmt       = flag.String("smt", "", ".smt.val (required)")
)


func updateGroup(arg prog.Arg, t map[uint32]uint8, e string) {
        /* for sendmsg */
    group := arg.(*prog.GroupArg)

	for i := 0; i < len(group.Inner); i++ {
		if a, ok := group.Inner[i].(*prog.PointerArg); ok {
//	        fmt.Fprintf(os.Stdout, "Pointer %v\n", i)
		    if _, ok := a.Res.(*prog.ConstArg); ok {
//	            fmt.Fprintf(os.Stdout, "\tConst %v\n", i)
		    } else if _, ok := a.Res.(*prog.GroupArg); ok {
//	            fmt.Fprintf(os.Stdout, "\tGroup %v\n", i)
	            group2 := a.Res.(*prog.GroupArg)
	            for j := 0; j < len(group2.Inner); j++ {
		            if _, ok := group2.Inner[j].(*prog.GroupArg); ok {
//	                    fmt.Fprintf(os.Stdout, "\t\tGroup: %d\n", j)

	                    group3 := group2.Inner[j].(*prog.GroupArg)
	                    for k := 0; k < len(group3.Inner); k++ {
		                    if _, ok := group3.Inner[k].(*prog.GroupArg); ok {
//	                            fmt.Fprintf(os.Stdout, "\t\t\tGroup: %d\n", k)
		                    } else if _, ok := group3.Inner[k].(*prog.DataArg); ok {
//	                            fmt.Fprintf(os.Stdout, "\t\t\tData\n")
		                    } else if _, ok := group3.Inner[k].(*prog.ConstArg); ok {
//	                            fmt.Fprintf(os.Stdout, "\t\t\tConst %v\n", k)
		                    } else if q, ok := group3.Inner[k].(*prog.PointerArg); ok {
//	                            fmt.Fprintf(os.Stdout, "\t\t\tPointer %v\n", k)

		                        if z, ok := q.Res.(*prog.DataArg); ok {
	                                p := z.Data()
	                                fmt.Fprintf(os.Stdout, "\t\t\t\tData size:%v\n", len(p))
			                        updateData(q.Res, t, e)
		                        }

		                    } else {
	                            fmt.Fprintf(os.Stdout, "\t\t\telse\n")
	                        }
	                    }
		            } else if _, ok := group2.Inner[j].(*prog.DataArg); ok {
	                    fmt.Fprintf(os.Stdout, "\t\tData\n")
		            } else if _, ok := group2.Inner[j].(*prog.ConstArg); ok {
	                    fmt.Fprintf(os.Stdout, "\t\tConst %v\n", j)
		            } else if _, ok := group2.Inner[j].(*prog.PointerArg); ok {
	                    fmt.Fprintf(os.Stdout, "\t\tPointer %v\n", j)
		            } else {
	                    fmt.Fprintf(os.Stdout, "\t\telse\n")
	                }
	            }

		    } else if _, ok := a.Res.(*prog.DataArg); ok {
	            fmt.Fprintf(os.Stdout, "\tData %v\n", i)
		    } else if _, ok := a.Res.(*prog.PointerArg); ok {
	            fmt.Fprintf(os.Stdout, "\tPointer %v\n", i)
		    } else {
	            fmt.Fprintf(os.Stdout, "\tElse %v\n", i)
		    }
//		} else if _, ok := group.Inner[i].(*prog.ConstArg); ok {
//	        fmt.Fprintf(os.Stdout, "Const %v\n", i)
//		} else if _, ok := group.Inner[i].(*prog.GroupArg); ok {
//	        fmt.Fprintf(os.Stdout, "grou: %d\n", i)
//		} else if _, ok := group.Inner[i].(*prog.DataArg); ok {
//	        fmt.Fprintf(os.Stdout, "Data\n")
//		} else {
//	        fmt.Fprintf(os.Stdout, "else\n")
	    }
	}
}

func updateConst(arg prog.Arg, t map[uint32]uint8, e string) {
    i := arg.(*prog.ConstArg)

	v := make([]byte, i.Size()) // 8 bytes

    // bgnd value
//	if e != "" {
//		if val, err := strconv.ParseInt(e, 0, 32); err == nil {
//	        for j := 0; j < int(i.Size()); j++ {
//		        v[j] = byte(uint8(val))
//		    }
//		}
//	}

	for i, j := range t {
		v[i] = byte(j)
	}

	i.Val = uint64(binary.LittleEndian.Uint64(v))
	//fmt.Fprintf(os.Stdout, "after: %v\n", i.Val)
}

func updateData(arg prog.Arg, t map[uint32]uint8, e string) {
    buffer := arg.(*prog.DataArg)
	p := buffer.Data()

    // bgnd value
	if e != "" {
		if val, err := strconv.ParseInt(e, 0, 32); err == nil {
	        for i := 0; i < len(p); i++ {
		        p[i] = uint8(val)
		    }
		}
	}

	for i, j := range t {
	    if uint32(len(p)) < i {
		    fmt.Fprintf(os.Stdout, "[ERROR!!] updateData access:%d, len:%d\n", i, len(p))
		    break
	    }
		p[i] = j
	}
	//fmt.Fprintf(os.Stdout, "after: %v\n", p)
}

func setNewArg(callName string, name string, arg prog.Arg, symName string, valuePairs string) {

    // set up temp buf `t`
	t := make(map[uint32]uint8)
	else_val := ""
	for _, elem := range strings.Split(valuePairs, ", ") {

		//fmt.Fprintf(os.Stdout, "values %v\n", elem)
		pair := strings.Split(elem, " -> ")
		if strings.TrimSpace(pair[0]) == "else" {
		    else_val = pair[1]
			continue
		}

        // extract offset
		if off, err := strconv.ParseInt(pair[0], 0, 32); err == nil {

            // extract value
			if val, err := strconv.ParseInt(pair[1], 0, 32); err == nil {
				t[uint32(off)] = uint8(val)
			} else {
				fmt.Fprintf(os.Stdout, "[ERROR!!] value %v ParseInt failure: %v\n", val, err)
			}

		} else {
			fmt.Fprintf(os.Stdout, "[ERROR!!] off %v ParseInt failure: %v\n", off, err)
		}
	}


	if a, ok := arg.(*prog.ConstArg); ok {
		updateConst(a, t, else_val)
	} else if a, ok := arg.(*prog.PointerArg); ok {
		if _, ok := a.Res.(*prog.ConstArg); ok {
			updateConst(a.Res, t, else_val)
		} else if _, ok := a.Res.(*prog.GroupArg); ok {
			updateGroup(a.Res, t, else_val)
		} else if _, ok := a.Res.(*prog.DataArg); ok {
			updateData(a.Res, t, else_val)
		} else {
			fmt.Fprintf(os.Stderr, "[ERROR!!] Unknwon case PointerArg for \"%s\" %d\n", name, a.Res)
		}
	} else {
			fmt.Fprintf(os.Stderr, "[ERROR!!] Unknown case for \"%s\" %v\n", name, arg)
	}
}

func modifyArgs(meta *prog.Syscall, args []prog.Arg, valfile string) {

    // parsing .val 
	smtData, err := ioutil.ReadFile(valfile)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to read %s file: %v\n", valfile, err)
		os.Exit(1)
	}

	visit := make(map[string]bool)
	rand.Seed(time.Now().UnixNano())
	re := regexp.MustCompile("_a._")

	lines := strings.Split(string(smtData), "\n")
	for _, line := range lines {

		if strings.TrimSpace(line) == "" {
			continue
		}

		if visit[re.FindString(line)] == true {  // how to handle more than one "_aX_"
			if rand.Int()%2 == 0 {      // probability solution right now
				continue
			}
		} else {
			visit[re.FindString(line)] = true
		}

		symName := strings.SplitN(line, " ", 2)[0] // e.g., v0___NR_setsockopt_a3_0_0x433..
		values := strings.SplitN(line, " ", 2)[1]
		valuePairs := strings.Replace(strings.Replace(values, "[", "", 1), "]", "", 1) // e.g., 0 -> 4, 15 -> 1, 1 -> 0,
        arg_idx, _ := strconv.ParseInt(re.FindString(line)[2:3], 10, 32)
		//fmt.Fprintf(os.Stdout, "line:%v idx:%d\n", line, arg_idx)

		setNewArg(meta.CallName, meta.Name, args[arg_idx], symName, valuePairs)
	}
}

func main() {
	flag.Parse()
	if *flagProg == "" || strings.Contains(*flagProg, ".prog.done") == false {
		flag.PrintDefaults()
		os.Exit(1)
	}
	if *flagSmt == "" || strings.Contains(*flagSmt, ".smt2.val") == false {
		flag.PrintDefaults()
		os.Exit(1)
	}
	target, err := prog.GetTarget(*flagOS, *flagArch)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v", err)
		os.Exit(1)
	}


    // 1. extract original prog and smt2 values
	oldProg, err := ioutil.ReadFile(*flagProg)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to read prog file: %v\n", err)
		os.Exit(1)
	}
	p, err := target.Deserialize(oldProg)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to deserialize the program: %v\n", err)
		os.Exit(1)
	}
	//fmt.Fprintf(os.Stdout, "program: %s\n", p.Serialize())


    // 2. find and mutate args
	smtCallName := strings.Split(strings.Split(string(*flagSmt)[:len(*flagSmt)-21], "/")[1], "-")[0]
	//fmt.Fprintf(os.Stdout, "%s\n", smtCallName)
    for idx := 0; idx < len(p.Calls); idx++ {
		c := p.Calls[idx]
	    if c.Meta.CallName == smtCallName {
            modifyArgs(c.Meta, c.Args, *flagSmt)
	    }
	}


    // 3. write new prog
	var newProg bytes.Buffer
	newProg.WriteString(strings.Replace(*flagSmt, "val", "P", 2))
	//fmt.Fprintf(os.Stdout, "%s\n", newProg.String())

	file, err := os.Create(newProg.String())
	if err != nil {
		fmt.Fprintf(os.Stderr, "[ERROR!!] fail to create file %s\n", newProg.String())
		return
	}
	file.WriteString("# ")
	//file.WriteString(p.NameCall)
	file.WriteString("\n")
	num, err := file.Write(p.Serialize())
	if err != nil {
	    file.Close()
		fmt.Fprintf(os.Stderr, "[ERROR!!] fail to write file %s (%d bytes)\n", num, newProg.String())
		return
	}
	file.WriteString("###")
	file.Close()
}
