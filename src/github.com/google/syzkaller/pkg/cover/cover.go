// Copyright 2015 syzkaller project authors. All rights reserved.
// Use of this source code is governed by Apache 2 LICENSE that can be found in the LICENSE file.

// Package cover provides types for working with coverage information (arrays of covered PCs).
package cover

type Cover map[uint32]struct{}

func (cov *Cover) Merge(raw []uint32) {
	c := *cov
	if c == nil {
		c = make(Cover)
		*cov = c
	}
	for _, pc := range raw {
		c[pc] = struct{}{}
	}
}

func (cov Cover) Serialize() []uint32 {
	res := make([]uint32, 0, len(cov))
	for pc := range cov {
		res = append(res, pc)
	}
	return res
}

func RestorePC(pc uint32, base uint32) uint64 {
	return uint64(base)<<32 + uint64(pc)
}

func (cov Cover) Diff(c1 []uint32) []uint32 {
	if len(cov) == 0 {
		return nil
	}
	if len(c1) == 0 {
		return nil
	}
	var res []uint32
	for _, pc := range c1 {
		if _, ok := cov[pc]; ok {
			continue
		}
		res = append(res, pc)
	}
	return res
}
