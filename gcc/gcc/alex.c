
#include <stdio.h>
#include "config.h"
#include "system.h"
#include "coretypes.h"
#include "backend.h"
#include "tree.h"
#include "gimple.h"
#include "tree-pass.h"
#include "ssa.h"
#include "gimple-pretty-print.h"
#include "diagnostic-core.h"
#include "langhooks.h"
#include "cfganal.h"
#include "cfgloop.h"
#include "gimple-iterator.h"
#include "tree-cfg.h"
#include "cfghooks.h"
#include "tree-into-ssa.h"
#include "tree-dfa.h"
#include "tree-ssa.h"
#include "cgraph.h"
#include "gimple-builder.h"



//static void
//transform_type2 (gimple_seq seq, tree cmd, long num_elems, tree type, tree array, tree fp)
//{
//  long i;
//  gimple_stmt_iterator gsi = gsi_start (seq);
//
//  for (i=0; i<num_elems; i++)
//  {
//    tree arr_idx = build_int_cst (TREE_TYPE(cmd), i);
//    tree t_label = create_artificial_label (UNKNOWN_LOCATION);
//    tree f_label = create_artificial_label (UNKNOWN_LOCATION);
//
//    // 0. gimple_cond
//    gimple *cond = gimple_build_cond (EQ_EXPR, cmd, arr_idx, t_label, f_label); // (code, lhs, rhs, t_label, f_label)
//    gsi_insert_after (&gsi, cond, GSI_NEW_STMT);  
//
//    // 1. gimple_label For TRUE
//    gsi_insert_after (&gsi, gimple_build_label (t_label), GSI_NEW_STMT);  
//
//    // 2. gimple_assign
//    tree t = build4 (ARRAY_REF, TREE_TYPE (type), array, arr_idx, NULL_TREE, NULL_TREE);
//    t = build1 (ADDR_EXPR, TREE_TYPE (t), t);
//    gimple *assign = gimple_build_assign (fp, ADDR_EXPR, t, NULL_TREE, NULL_TREE);
//    gsi_insert_after (&gsi, assign, GSI_NEW_STMT);  
//
//    // 3. gimple_label For FALSE
//    gsi_insert_after (&gsi, gimple_build_label (f_label), GSI_NEW_STMT);  
//  }
//
////  gsi = gsi_start (seq);
////  gsi_remove (&gsi, false);
//}


static void
transform_type2 (gimple *call_stmt, gimple *fn_def, tree cmd, long num_elems, tree addr_expr_type, tree arr_ref_type, tree array, tree fp)
{
  // to be removed
  static bool visited = false; 
  if (visited)  
    return ;
  visited = true;


  long i;
  gimple_stmt_iterator gsi; 
  basic_block start_bb = gimple_bb (fn_def);
  basic_block end_bb = NULL;
  basic_block head_bb = NULL;
  basic_block true_bb = NULL;
  basic_block fallthru_bb = NULL;

  // split current block
  // if (fn_def == fn_def->prev)
  edge e = split_block (start_bb, fn_def->prev);  // 1. new's succs = old's succs .. 2. old's succ = new .. 3. new's preds = old
  if (single_succ_p (start_bb))
  {
    end_bb = single_succ (start_bb);
    remove_edge_raw (e);
  }
  else  // split failure
    return;


  for (i=0; i<num_elems; i++)
  {
    int uid;
    tree arr_idx;
    tree t_label;
    tree f_label;
    gimple *cond_stmt = NULL;
    gimple *assign_stmt = NULL;


    // 1. build gimple_cond
    arr_idx = build_int_cst (TREE_TYPE(cmd), i);
    t_label = create_artificial_label (UNKNOWN_LOCATION);
    f_label = create_artificial_label (UNKNOWN_LOCATION);
    cond_stmt = gimple_build_cond (EQ_EXPR, cmd, arr_idx, t_label, f_label); // (code, lhs, rhs, t_label, f_label)

    // 2. build gimple_assign
    tree t = build4 (ARRAY_REF, TREE_TYPE (arr_ref_type), array, arr_idx, NULL_TREE, NULL_TREE);
    t = build1 (ADDR_EXPR, TREE_TYPE (addr_expr_type), t);
    //assign_stmt = gimple_build_assign (fp, ADDR_EXPR, t, NULL_TREE, NULL_TREE);
    assign_stmt = gimple_build_assign (make_ssa_name (TREE_TYPE (fp)), ADDR_EXPR, t, NULL_TREE, NULL_TREE);


    // 0. build head bb
    head_bb = create_basic_block (cond_stmt, start_bb);  // create_bb()... NOT handle any preds and succs (only next_bb and prev_bb)
    gimple_set_bb (cond_stmt, head_bb); // add STMT to BB
    //redirect_edge_succ (e, head_bb);
    if (current_loops)
      add_bb_to_loop (head_bb, current_loops->tree_root);
    e = make_edge (start_bb, head_bb, EDGE_FALLTHRU);

    // 1. build true bb
    true_bb = create_basic_block (assign_stmt, head_bb);  
    gimple_set_bb (assign_stmt, true_bb);
    if (current_loops)
      add_bb_to_loop (true_bb, current_loops->tree_root);
    e = make_edge (head_bb, true_bb, EDGE_TRUE_VALUE);

    uid = LABEL_DECL_UID (t_label);
    if (uid == -1)
	  {
	    unsigned old_len =
	      vec_safe_length (label_to_block_map_for_fn (cfun));
	    LABEL_DECL_UID (t_label) = uid = cfun->cfg->last_label_uid++;
	    if (old_len <= (unsigned) uid)
	      {
	        unsigned new_len = 3 * uid / 2 + 1;

	        vec_safe_grow_cleared (label_to_block_map_for_fn (cfun),
				      new_len);
	      }
	  }
    (*label_to_block_map_for_fn (cfun))[uid] = true_bb;


    // 2. build fallthru bb
    fallthru_bb = create_empty_bb (head_bb);  
    if (current_loops)
      add_bb_to_loop (fallthru_bb, current_loops->tree_root);
    e = make_edge (head_bb, fallthru_bb, EDGE_FALSE_VALUE);
    e = make_edge (true_bb, fallthru_bb, EDGE_FALLTHRU);

    uid = LABEL_DECL_UID (f_label);
    if (uid == -1)
	  {
	    unsigned old_len =
	      vec_safe_length (label_to_block_map_for_fn (cfun));
	    LABEL_DECL_UID (f_label) = uid = cfun->cfg->last_label_uid++;
	    if (old_len <= (unsigned) uid)
	      {
	        unsigned new_len = 3 * uid / 2 + 1;

	        vec_safe_grow_cleared (label_to_block_map_for_fn (cfun),
				      new_len);
	      }
	  }
    (*label_to_block_map_for_fn (cfun))[uid] = fallthru_bb;

    // 3. finalize
    gimple_cond_set_true_label (as_a <gcond *>(cond_stmt), NULL_TREE);
    gimple_cond_set_false_label (as_a <gcond *>(cond_stmt), NULL_TREE);

    // merge start_bb and head bb
    if (can_merge_blocks_p (start_bb, head_bb))
      merge_blocks (start_bb, head_bb);

    start_bb = fallthru_bb;
  }

  e = make_edge (fallthru_bb, end_bb, EDGE_FALLTHRU);

  // 2. merge fallthru_bb and end bb
  if (can_merge_blocks_p (fallthru_bb, end_bb))
    merge_blocks (fallthru_bb, end_bb);

  // insert phi node
  gphi *phi = create_phi_node (fp, end_bb);
	//add_phi_arg (phi, var, e, UNKNOWN_LOCATION);


//  gsi_remove (&gsi, false);

}

static void
analyze_fp_call_2 (gimple_seq call_stmt)
{
  int WORD_SIZE = 8;

	/// 1st stmt 
  if (!is_gimple_call(call_stmt))
    return;
  
	//tree fndecl = gimple_call_fndecl (call_stmt);  // FUNCTION_DECL 
	tree fn = gimple_call_fn (call_stmt);  // if SSA_NAME... indirect call
	if (!fn || TREE_CODE(fn) != SSA_NAME)  // there is no VAR_DECL.. due to after ssa
    return;
	
	/// 2nd stmt 
  //gimple *ssa_def = SSA_VAL (fn);  
  gimple *ssa_def = SSA_NAME_DEF_STMT(fn);
  if (!is_gimple_assign (ssa_def)) 
      return;


  if (gimple_assign_rhs_code (ssa_def) == ARRAY_REF 
        || gimple_assign_rhs_code (ssa_def) == COMPONENT_REF) 
  {
    tree ref = gimple_assign_rhs1 (ssa_def);  // ARRAY_REF "ucma_cmd_table[cmd]", COMPONENT_REF "fn->cb"

    if (TREE_CODE (ref) == COMPONENT_REF) 
    {
	      // tree offset = unshare_expr (component_ref_field_offset (ref));
        tree mem_ref = TREE_OPERAND (ref, 0);  // MEM_REF "fn"
        tree field = TREE_OPERAND (ref, 1);  // FIELD_DECL "cb"
//	      tree factor = size_int (DECL_OFFSET_ALIGN (field) / BITS_PER_UNIT);
//
        if (TREE_CODE (mem_ref) == MEM_REF)
        {
          tree fp = TREE_OPERAND (mem_ref, 0);  // SSA_NAME "fn"
          tree type = TREE_TYPE (fp);

	        if (TREE_CODE(fp) == SSA_NAME)  
	        {
	          /// 3rd stmt 
            gimple *fn_def = SSA_NAME_DEF_STMT(fp);  
            if (is_gimple_assign (fn_def) && gimple_assign_rhs_code (fn_def) == ADDR_EXPR ) 
            {
              tree addr_expr = gimple_assign_rhs1 (fn_def);  // ... ADDR_EXPR
              tree arr_ref = TREE_OPERAND (addr_expr, 0);  // ARRAY_REF reply_funcs[argc]

              if (TREE_CODE (arr_ref) == ARRAY_REF) 
              {
	              int num_elems = 0; // array_size
                tree array = TREE_OPERAND (arr_ref, 0);  // VAR_DECL reply_funcs 
                tree cmd = TREE_OPERAND (arr_ref, 1);  // SSA_NAME argc

	              if (array != NULL_TREE && TREE_CODE (TREE_TYPE (array)) == ARRAY_TYPE)
	              {
	                if (TREE_CODE (array) == COMPONENT_REF)  // e.g. 
	                  return;

	                tree array_size = DECL_SIZE (array);
	                if (TREE_CODE (array_size) == INTEGER_CST && !integer_zerop (array_size))
	                {
	                  num_elems = (tree_to_uhwi(array_size)/BITS_PER_UNIT)/WORD_SIZE;
	                }

                  transform_type2(call_stmt, fn_def, cmd, num_elems, addr_expr, arr_ref, array, fp);
	              }
              }

            }

          }
        }
    }
    else if (TREE_CODE (ref) == ARRAY_REF) 
    {
    }
  }
}

//namespace {

const pass_data pass_data_alexkidd =
{
  GIMPLE_PASS, /* type */
  "alexkidd", /* name */
  OPTGROUP_NONE, /* optinfo_flags */
  TV_NONE, /* tv_id */
  PROP_ssa, /* properties_required */
  0, /* properties_provided */
  0, /* properties_destroyed */
  0, /* todo_flags_start */
  0, /* todo_flags_finish */
};

class pass_alexkidd : public gimple_opt_pass
{
public:
  pass_alexkidd (gcc::context *ctxt)
    : gimple_opt_pass (pass_data_alexkidd, ctxt)
  {}

  /* opt_pass methods: */
  virtual bool gate (function *) { return true; }
  virtual unsigned int execute (function *);
};

unsigned int
pass_alexkidd::execute (function *fun)
{
  /* NOTE!!! this pass is after ssa */

//  if (flag_alex)
//  {
	  //printf ("Ktkim: \"-falex\" flag is not enabled\n");
    return 0;
//  }

  basic_block bb;
  gimple_stmt_iterator gsi;
  //struct cgraph_node *node;

  //node = cgraph_node::get (current_function_decl);
  //struct function *fn = DECL_STRUCT_FUNCTION (current_function_decl);

  //push_cfun (DECL_STRUCT_FUNCTION (node->decl));
  FOR_EACH_BB_FN (bb, fun) 
  {
    for (gsi = gsi_start_bb (bb); !gsi_end_p (gsi); gsi_next (&gsi))
    {
      /* function pointer call (type 2) */
	    analyze_fp_call_2 (gsi_stmt (gsi));
	  }
	}
	//pop_cfun ();

	return 0;
}

//} // anon namespace

gimple_opt_pass *
make_pass_alexkidd (gcc::context *ctxt)
{
  return new pass_alexkidd (ctxt);
}

