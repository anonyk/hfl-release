
#include <stdio.h>
#include "config.h"
#include "system.h"
#include "coretypes.h"
#include "backend.h"
#include "tree.h"
#include "gimple.h"
#include "tree-pass.h"
#include "ssa.h"
#include "gimple-pretty-print.h"
#include "diagnostic-core.h"
#include "langhooks.h"
#include "cfganal.h"
#include "gimple-iterator.h"
#include "tree-cfg.h"
#include "tree-into-ssa.h"
#include "tree-dfa.h"
#include "tree-ssa.h"
#include "cgraph.h"
#include "gimple-builder.h"



static void
transform_type1(gimple_seq seq, tree cmd, long num_elems, tree type, tree array, tree ret, unsigned num_args, tree global_var)
{
  long i, j;
  gimple_stmt_iterator gsi = gsi_start (seq);

  // for the label of covergence
  tree end_label = create_artificial_label (UNKNOWN_LOCATION);


  for (i=0; i<num_elems; i++)
  {
    tree val = build_int_cst (TREE_TYPE(cmd), i);
    tree t_label = create_artificial_label (UNKNOWN_LOCATION);
    tree f_label = create_artificial_label (UNKNOWN_LOCATION);

    // BB_0. gimple_cond
    gimple *cond = gimple_build_cond (EQ_EXPR, cmd, val, t_label, f_label); // (code, lhs, rhs, t_label, f_label)
    gsi_insert_before (&gsi, cond, GSI_SAME_STMT);  

    // BB_1. gimple_label For TRUE
    gsi_insert_before (&gsi, gimple_build_label (t_label), GSI_SAME_STMT);  

    // BB_1. gimple_assign .. global var test
    gimple *assign_stmt = gimple_build_assign (global_var, ADDR_EXPR, val, NULL_TREE, NULL_TREE);
    gsi_insert_before (&gsi, assign_stmt, GSI_SAME_STMT);  

    // BB_1. gimple_assign
//    tree t = build4 (ARRAY_REF, TREE_TYPE (type), array, val, NULL_TREE, NULL_TREE);
//    gimple *assign = build_assign (ARRAY_REF, t, NULL_TREE, NULL_TREE);
//    gsi_insert_before (&gsi, assign, GSI_SAME_STMT);  

    // BB_1. gimple_call
//    gcall *call = gimple_build_call (gimple_assign_lhs (assign), num_args);
//    gimple_call_set_lhs (call, ret);
//    for (j=0; j<num_args; j++)
//      gimple_call_set_arg (call, j, gimple_call_arg(seq, j));
//    gsi_insert_before (&gsi, call, GSI_SAME_STMT);  

    // BB_1. gimple_goto
    gsi_insert_before (&gsi, gimple_build_goto (end_label), GSI_SAME_STMT);  

    // BB_2. gimple_label For FALSE
    gsi_insert_before (&gsi, gimple_build_label (f_label), GSI_SAME_STMT);  
  }

  // 6. gimple_label
  gsi_insert_before (&gsi, gimple_build_label (end_label), GSI_SAME_STMT);  

//  gsi_remove (&gsi, false);
}


static void
analyze_fp_call_1 (gimple_seq call_stmt, tree global_var)
{
  int WORD_SIZE = 8;

	/// 1st stmt 
  if (!is_gimple_call (call_stmt))
    return;
  
  // Return
  //int flags = gimple_call_flags (call_stmt);
//  tree ret = gimple_call_lhs (call_stmt);  // VAR_DECL for return
//  if (ret != NULL_TREE && TREE_CODE (ret) == VAR_DECL)
//	    printf (" return \"%s\"\n", get_name(ret));


	//tree fndecl = gimple_call_fndecl (call_stmt);  // FUNCTION_DECL 
	tree fn = gimple_call_fn (call_stmt);  // if SSA_NAME... indirect call
	if (!fn || TREE_CODE(fn) != SSA_NAME)  // there is no VAR_DECL.. due to after ssa
	  return ;

  //gimple *ssa_def = SSA_VAL (fn);  
  gimple *ssa_def = SSA_NAME_DEF_STMT(fn);
  if (!is_gimple_assign (ssa_def)) 
      return;


  if (gimple_assign_rhs_code (ssa_def) == ARRAY_REF 
        || gimple_assign_rhs_code (ssa_def) == COMPONENT_REF)
  {
    tree ref = gimple_assign_rhs1 (ssa_def);  // ARRAY_REF "ucma_cmd_table[cmd]", COMPONENT_REF "fn->cb"

    if (TREE_CODE (ref) == ARRAY_REF) 
    {
        //tree addr = get_base_address (rhsop1)
	      int num_elems = 0; // array_size
        tree array = TREE_OPERAND (ref, 0);  // VAR_DECL "ucma_cmd_table"... can be COMPONENT_REF (e.g.hda_controller.c)
        tree cmd = TREE_OPERAND (ref, 1);  // VAR_DECL "cmd" or SSA_NAME "hdr.cmd" ... its type doesn't matter.

	      if (TREE_CODE (TREE_TYPE (array)) == ARRAY_TYPE)
	      {
	        if (TREE_CODE (array) == COMPONENT_REF)  // e.g. hda_controller.c
	          return;

	        tree array_size = DECL_SIZE (array);
	        //tree array_size2 = array_ref_element_size (ref);
	        //tree array_size2 = array_ref_low_bound (ref);
	        if (array_size != NULL_TREE && TREE_CODE (array_size) == INTEGER_CST && !integer_zerop (array_size))
	        {
	          num_elems = (tree_to_uhwi(array_size)/BITS_PER_UNIT)/WORD_SIZE;
	        }

          if (num_elems)
            transform_type1(call_stmt, cmd, num_elems, ref, array, gimple_call_lhs (call_stmt), gimple_call_num_args(call_stmt), global_var);
	      }
    }
    else if (TREE_CODE (ref) == COMPONENT_REF) 
    {
	    // tree offset = unshare_expr (component_ref_field_offset (ref));
      tree mem_ref = TREE_OPERAND (ref, 0);  // MEM_REF "fn"
      tree field = TREE_OPERAND (ref, 1);  // FIELD_DECL "cb"
//	    tree factor = size_int (DECL_OFFSET_ALIGN (field) / BITS_PER_UNIT);

      if (TREE_CODE (mem_ref) == MEM_REF)
      {
        tree fp = TREE_OPERAND (mem_ref, 0);  // SSA_NAME "fn"
        tree type = TREE_TYPE (fp);

	      if (TREE_CODE(fp) == SSA_NAME)  
	      {
	        /// 3rd stmt 
          gimple *fn_def = SSA_NAME_DEF_STMT(fp);  
          if (is_gimple_assign (fn_def) && gimple_assign_rhs_code (fn_def) == ADDR_EXPR ) 
          {
            tree addr_expr = gimple_assign_rhs1 (fn_def);  // ... ADDR_EXPR
            tree arr_ref = TREE_OPERAND (addr_expr, 0);  // ARRAY_REF reply_funcs[argc]

            if (TREE_CODE (arr_ref) == ARRAY_REF) 
            {
	            int num_elems = 0; // array_size
              tree array = TREE_OPERAND (arr_ref, 0);  // VAR_DECL reply_funcs 
              tree cmd = TREE_OPERAND (arr_ref, 1);  // SSA_NAME argc

	            if (array != NULL_TREE && TREE_CODE (TREE_TYPE (array)) == ARRAY_TYPE)
	            {
	              if (TREE_CODE (array) == COMPONENT_REF)  // e.g. only xfrm_trans_reinject
	              {
	                return;
	              }

	              tree array_size = DECL_SIZE (array);
	              if (array_size != NULL_TREE && TREE_CODE (array_size) == INTEGER_CST && !integer_zerop (array_size))
	              {
	                num_elems = (tree_to_uhwi(array_size)/BITS_PER_UNIT)/WORD_SIZE;
	              }

                //transform_type2(call_stmt, fn_def, cmd, num_elems, addr_expr, arr_ref, array, fp);
	            }
            }

          }
        }
      }
    }
  }
}


// insert ifs right before switches to facilitate exploration
static void
insert_ifcond (gimple_seq stmt, tree global_var)
{
  int i;
  gimple_stmt_iterator gsi = gsi_start (stmt);

  if (gimple_code (stmt) != GIMPLE_SWITCH)
    return;

  // heuristic to avoid corruption
  if (current_function_decl && !strcmp(get_name(current_function_decl), "init_intel_cacheinfo") )
	  return;
  if (current_function_decl && !strcmp(get_name(current_function_decl), "amd_cpuid4") )
	  return;
  if (current_function_decl && !strcmp(get_name(current_function_decl), "early_init_intel") )
	  return;
  if (current_function_decl && !strcmp(get_name(current_function_decl), "init_intel") )
	  return;
  if (current_function_decl && !strcmp(get_name(current_function_decl), "intel_tlb_lookup") )
	  return;
  if (current_function_decl && !strcmp(get_name(current_function_decl), "intel_workarounds") )
	  return;
  if (current_function_decl && !strcmp(get_name(current_function_decl), "probe_xeon_phi_r3mwait") )
	  return;

  gswitch *switch_stmt = as_a <gswitch *> (stmt);
  int n_labels = gimple_switch_num_labels (switch_stmt);
  if (n_labels < 4)
    return;

  // return if first stmt of seq .. TODO
  if (switch_stmt->prev->next == NULL)
    return;

  tree cmd = gimple_switch_index (switch_stmt);

  for (i=1; i<n_labels; i++)
  {
    tree case_label_expr = gimple_switch_label (switch_stmt, i);  // CASE_LABEL_EXPR
    //tree label_decl = CASE_LABEL (case_label_expr);   // LABEL_DECL
    if (CASE_HIGH (case_label_expr) == NULL_TREE && CASE_LOW (case_label_expr) != NULL_TREE)
    {
      //tree val = build_int_cst (TREE_TYPE(cmd), i);
      tree val = CASE_LOW (case_label_expr);
      tree t_label = create_artificial_label (UNKNOWN_LOCATION);
      tree f_label = create_artificial_label (UNKNOWN_LOCATION);

      // BB_0. gimple_cond
      gimple *cond = gimple_build_cond (EQ_EXPR, cmd, val, t_label, f_label); // (code, lhs, rhs, t_label, f_label)
      gsi_insert_before (&gsi, cond, GSI_SAME_STMT);  

      // BB_1. gimple_label (for TRUE)
      gsi_insert_before (&gsi, gimple_build_label (t_label), GSI_SAME_STMT); 

      // BB_1. gimple_assign
      gimple *assign_stmt = gimple_build_assign (global_var, val);
      //gimple *assign_stmt = gimple_build_assign (global_var, ADDR_EXPR, val, NULL_TREE, NULL_TREE);
      gsi_insert_before (&gsi, assign_stmt, GSI_SAME_STMT);  // causing error
      
      // BB_2. gimple_label (for FALSE)
      gsi_insert_before (&gsi, gimple_build_label (f_label), GSI_SAME_STMT);  
    }
  }


  // For each case value
//  while (tree_vals->length() > 0)
//  {
//    tree val = tree_vals->pop (); // must be INTEGER_CST??
//
//    //tree integer_cst = build_int_cst (TREE_TYPE(cmd), i);
//    tree t_label = create_artificial_label (UNKNOWN_LOCATION);
//    tree f_label = create_artificial_label (UNKNOWN_LOCATION);
//
//    // 0. gimple_cond
//    gimple *cond = gimple_build_cond (EQ_EXPR, cmd, val, t_label, f_label); // (code, lhs, rhs, t_label, f_label)
//    gsi_insert_before (&gsi, cond, GSI_SAME_STMT);  
//
//    // 1. gimple_label (for TRUE)
//    gsi_insert_before (&gsi, gimple_build_label (t_label), GSI_SAME_STMT); 
//
//    // 2. gimple_assign
////    tree global_var = build_decl (UNKNOWN_LOCATION, VAR_DECL, NULL_TREE, uint32_type_node);  // create global var
////    //varpool_node *node = varpool_node::add (var);
////    gimple *assign = gimple_build_assign (global_var, val);
////    gsi_insert_before (&gsi, assign, GSI_SAME_STMT);  
//
//    // 3. gimple_label (for FALSE)
//    gsi_insert_before (&gsi, gimple_build_label (f_label), GSI_SAME_STMT);  
//
//  }

}


static gimple *
find_gimple_label (tree label)
{
  gimple_stmt_iterator gsi;
  gimple_seq body = gimple_body (current_function_decl);

  for (gsi = gsi_start (body); !gsi_end_p (gsi); gsi_next (&gsi))
  {
    gimple *stmt = gsi_stmt (gsi);
    if (gimple_code (stmt) != GIMPLE_LABEL)
      continue;

    if (label == gimple_label_label (as_a <glabel *> (stmt)))
      return stmt;
	}

	return NULL;
}

static void
insert_cond_before_label (gimple_seq seq, tree cmd, tree val, tree t_label)
{
    gimple_stmt_iterator gsi = gsi_last (seq);  // ->prev GIMPLE_GOTO, GIMPLE_SWITCH, GIMPLE_LABEL
    gsi = gsi_start (seq);  // pointing to this gimple ... GIMPLE_LABEL

    tree f_label = create_artificial_label (UNKNOWN_LOCATION);
    gimple *cond = gimple_build_cond (EQ_EXPR, cmd, val, t_label, f_label); // (code, lhs, rhs, t_label, f_label)

    //gsi_insert_before (&gsi, cond, GSI_SAME_STMT);  
    gsi_insert_before (&gsi, gimple_build_label (f_label), GSI_SAME_STMT);  
}


static void
switch_to_ifcond (gimple_seq stmt)
{
  int i;
  auto_vec<gimple *, 64> gimple_gotos;
  gimple_stmt_iterator gsi;

  if (gimple_code (stmt) != GIMPLE_SWITCH)
    return;

  gswitch *switch_stmt = as_a <gswitch *> (stmt);
  int n_labels = gimple_switch_num_labels (switch_stmt);
  if (n_labels < 3)
    return;

  tree cmd = gimple_switch_index (switch_stmt);

  // 1. find the last GIMPLE_LABEL and GIMPLE_GOTOs
  gimple *end_label = NULL;
  for (i=0; i<n_labels; i++)
  {
    tree label_decl = CASE_LABEL (gimple_switch_label (switch_stmt, i));   // LABEL_DECL
    gimple *label = find_gimple_label (label_decl);
    gimple *prev_label = label->prev;
    if (prev_label == NULL || gimple_code (prev_label) != GIMPLE_GOTO)
      continue;

    // push all GIMPLE_GOTO
    gimple_gotos.quick_push (prev_label);

    gimple *l = find_gimple_label (gimple_goto_dest (prev_label));
    if (end_label != NULL && end_label != l) 
	      printf ("[ERR!!] different end_label"); // unlikely
  }

	printf ("NUM %d %d ", n_labels, gimple_gotos.length()); 

  // 2. insert GIMPLE_COND
  for (i=0; i<n_labels; i++)
  {
    tree case_label_expr = gimple_switch_label (switch_stmt, i);  // CASE_LABEL_EXPR
    tree label_decl = CASE_LABEL (case_label_expr);   // LABEL_DECL
    if (CASE_HIGH (case_label_expr) == NULL_TREE && CASE_LOW (case_label_expr) != NULL_TREE)
    {
      tree low = CASE_LOW (case_label_expr);
      gimple *gl = find_gimple_label (label_decl);
      //insert_cond_before_label (gl, cmd, low, label_decl);
	  }
	  
    while (gimple_gotos.length() > 0)
    {
      tree f_label = create_artificial_label (UNKNOWN_LOCATION);
      gimple *stmt = gimple_gotos.pop ();
      gsi = gsi_start (stmt);  
      //gsi_replace (&gsi, gimple_build_label (f_label), true); 
      ////gsi_insert_before (&gsi, gimple_build_label (f_label), GSI_SAME_STMT);  
    }
  }

  // change GIMPLE_GOTO to LABEL
//  while (gimple_gotos.length() > 0)
//  {
//    tree f_label = create_artificial_label (UNKNOWN_LOCATION);
//    gimple *stmt = gimple_gotos.pop ();
//    gsi = gsi_start (stmt);  
//    //gsi_insert_before (&gsi, gimple_build_label (f_label), GSI_SAME_STMT);  
//    gsi_replace (&gsi, gimple_build_label (f_label), true); 
//  }


  // 3. remove GIMPLE_SWITCH 
  //tree L = create_artificial_label (UNKNOWN_LOCATION);
  gsi = gsi_start(stmt);
  gsi_remove (&gsi, false); 
  //gsi_replace (&gsi, gimple_build_label (L), true); // delete GIMPLE_SWITCH


  // 3. remove last GIMPLE_LABEL
  if (end_label != NULL)
  {
    gsi = gsi_start(end_label);
    //gsi_remove (&gsi, false); 
  }
}

const pass_data pass_data_alexkidd_switch =
{
  GIMPLE_PASS, /* type */
  "alexkidd_switch", /* name */
  OPTGROUP_NONE, /* optinfo_flags */
  TV_NONE, /* tv_id */
  PROP_gimple_leh, /* properties_required */
  0, /* properties_provided */
  0, /* properties_destroyed */
  0, /* todo_flags_start */
  0, /* todo_flags_finish */
};

class pass_alexkidd_switch : public gimple_opt_pass
{
public:
  pass_alexkidd_switch (gcc::context *ctxt)
    : gimple_opt_pass (pass_data_alexkidd_switch, ctxt)
  {}

  /* opt_pass methods: */
  virtual bool gate (function *) { return true; }
  virtual unsigned int execute (function *);
};

unsigned int
pass_alexkidd_switch::execute (function *fun)
{
  /* NOTE!!! this pass is before ssa */

  if (flag_alex)
  {
	  //printf ("Ktkim: \"-falex\" flag is not enabled\n");
    return 0;
  }

  gimple_stmt_iterator gsi;
  gimple_seq body = gimple_body (current_function_decl);

  // refer to global variable (which is created in i386.c)
  //if (!varpool_node::get (global_name))
  tree global_name = get_identifier ("alex_global_var");
  tree global_var = build_decl (UNKNOWN_LOCATION, VAR_DECL, global_name, uint32_type_node);
  DECL_ARTIFICIAL (global_var) = 0;
  TREE_USED (global_var) = 1;
  //TREE_STATIC (global_var) = 1; // 1. intenal .. (or must be DECL_EXTERNAL)
  //TREE_SIDE_EFFECTS (global_var) = 1; 
  //TREE_THIS_VOLATILE (global_var) = 1; 
  DECL_EXTERNAL (global_var) = 1; // 2. extenal
  TREE_PUBLIC (global_var) = 1; // 2. extenal
  //DECL_INITIAL (global_var) = build_int_cst (NULL_TREE, 9);
  //DECL_WEAK (global_var) = 1;
  //symbol_table::change_decl_assembler_name (global_var, global_name);
  varpool_node::add (global_var);


  for (gsi = gsi_start (body); !gsi_end_p (gsi); gsi_next (&gsi))
  {
    /* function pointer call (type 1) */
	  analyze_fp_call_1 (gsi_stmt (gsi), global_var);
	}

  for (gsi = gsi_start (body); !gsi_end_p (gsi); gsi_next (&gsi))
  {
    /* handling switch */
	  insert_ifcond (gsi_stmt (gsi), global_var); // insertion version
	  //switch_to_ifcond (gsi_stmt (gsi));  // replacement version (but hard to find end label)
	}

	return 0;
}

gimple_opt_pass *
make_pass_alexkidd_switch (gcc::context *ctxt)
{
  return new pass_alexkidd_switch (ctxt);
}
