#!/bin/bash


CURDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
GCC_DIR=$CURDIR/../gcc

VERBOSE="V=1"
CONFIG_OPTION="--disable-bootstrap --enable-languages=c --prefix=$GCC_DIR/newgcc"

function setup {
    sudo apt-get install libmpc-dev gcc-multilib
}

setup

mkdir -p $GCC_DIR/build
mkdir -p $GCC_DIR/newgcc

rm -rf $GCC_DIR/newgcc/*
pushd $GCC_DIR/build >/dev/null
rm -rf *
CXXFLAGS=$CXXFLAGS CFLAGS=$CXXFLAGS ../configure $CONFIG_OPTION
make $VERBOSE -j`nproc`
make install
popd 

