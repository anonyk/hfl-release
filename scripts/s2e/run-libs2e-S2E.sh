#!/bin/bash -e

# echo "usage: ./run-libs2e-S2E.sh [saved_vm_num] [vm_pid]"

CURDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

. ../common.sh

MODE=release


DISK="-drive file=$DISKDIR/$IMAGE.s2e,format=s2e,cache=writeback"
NET="-net none"


LOADVM="-loadvm 1"
if [ -n "$1" ]; then 
    LOADVM="-loadvm $1"
fi

VMPID="vm.pid"
if [ -n "$2" ]; then 
    VMPID="vm.$2.pid"
fi

export S2E_CONFIG=$CURDIR/s2e-config.lua
#export S2E_SHARED_DIR=$S2EBUILD/libs2e-$MODE/x86_64-s2e-sotmmu/


#### self kill when time is up
function kill_QEMU {
    COUNT=100        ### edit
    for i in $(seq "$COUNT"); do
        sleep 1
        if [ ! -f "$VMPID" ]; then 
            exit 1
        fi
    done
    kill -9 `cat $VMPID`     ## kill process
}
kill_QEMU & 

LIBS2E=$S2EBUILD/libs2e-$MODE/x86_64-s2e-softmmu/libs2e.so

#set -x

LD_PRELOAD=$LIBS2E $QEMU $DISK \
    $MEM \
    $ENABLEKVM \
    $NET \
    $LOADVM \
    $GRAPHIC \
    -pidfile $VMPID \
    $S2E_OPTION \
    -serial file:serial.txt \
    2>&1 | tee s2e.log

rm $VMPID
