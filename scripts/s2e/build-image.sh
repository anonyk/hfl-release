#!/bin/bash -e

source ../common.sh

TMPDIR=.tmp-output

OPTION=

## refer to S2E build directory
S2E_INSTALL_ROOT=$S2EHOME/build/opt

## refer to linux kernel build directory (e.g., s2e-linux-kernel)
S2E_LINUX_KERNELS_ROOT=$S2EHOME/s2e-linux-kernel/


function setup {
    sudo apt-get install libguestfs-tools genisoimage python-pip python-magic xz-utils docker.io p7zip-full pxz libhivex-bin fuse jigdo-file
    sudo apt-get build-dep fakeroot linux-image-$(uname -r)
    sudo pip install jinja2

    sudo chmod +r /boot/vmlinuz*
    sudo usermod -a -G docker $(whoami)
    sudo usermod -a -G kvm $(whoami)
}

### 0. Prerequisite
function build {
    pushd $S2EHOME/guest-images >/dev/null 
    rm -rf $TMPDIR
    rm -rf output
    rm -rf .stamps
    S2E_INSTALL_ROOT=$S2E_INSTALL_ROOT S2E_LINUX_KERNELS_ROOT=$S2E_LINUX_KERNELS_ROOT make linux $OPTION -j`nproc`
    popd
}

### 1. Create disk image
function createImage {
    if [ ! -d $DISKDIR ]; then
	mkdir -p $DISKDIR
    fi
    $S2EBUILD/opt/bin/qemu-img create -f raw $DISKDIR/$IMAGE 10G
}

### 2. Running initial setup disk.img (done automatically by install_files.iso)
function setupImage {
    if [ ! -f $S2EGUESTIMG/$TMPDIR/debian-9.2.1-x86_64/install_files.iso ]; then
	echo "[ERR!!] $S2EGUESTIMG/$TMPDIR/debian-9.2.1-x86_64/install_files.iso NOT exist"; exit 1
    fi
    $S2EBUILD/opt/bin/qemu-system-x86_64 -m 1G $NOREBOOT $GRAPHIC $ENABLEKVM \
        -drive if=ide,index=0,file=$DISKDIR/$IMAGE,format=raw,cache=writeback \
        -cdrom $S2EGUESTIMG/$TMPDIR/debian-9.2.1-x86_64/install_files.iso -serial file:./serial.txt
}

### Copy stuffs to disk image
function copyToImage {
    cp $DISKDIR/$IMAGE $DISKDIR/$IMAGE.s2e

    sudo virt-copy-in -a $DISKDIR/$IMAGE.s2e $HFLHOME/build/s2e/*.deb /home/s2e
    sudo virt-copy-in -a $DISKDIR/$IMAGE.s2e samples/sources.list /home/s2e
    sudo virt-copy-in -a $DISKDIR/$IMAGE.s2e $S2EGUESTIMG/Linux/s2e_home/launch.sh \
       $S2EGUESTIMG/Linux/s2e_home/.bash_login /home/s2e/  ### for install kernel, systemtap, 
    sudo guestfish --rw -a $DISKDIR/$IMAGE.s2e -i mkdir /etc/systemd/system/getty@tty1.service.d/
    sudo virt-copy-in -a $DISKDIR/$IMAGE.s2e $S2EGUESTIMG/Linux/override.conf /etc/systemd/system/getty@tty1.service.d/

   # actually install systemtap and kernel and then automatically reboot
    $S2EBUILD/opt/bin/qemu-system-x86_64 -m 1G $NOREBOOT $ENABLEKVM \
        -drive if=ide,index=0,file=$DISKDIR/$IMAGE.s2e,format=raw,cache=writeback \
       -serial file:./serial.txt

    sudo virt-copy-in -a $DISKDIR/$IMAGE.s2e $S2EBUILD/opt/bin/guest-tools64/* /home/s2e/
    sudo virt-copy-in -a $DISKDIR/$IMAGE.s2e ./samples/launch.sh /home/s2e/   ### overwrite launch.sh from guest-tools64
}


#### Prerequisite
setup
build

#### Commands
createImage 
setupImage
copyToImage

