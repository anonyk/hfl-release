#!/bin/bash -e

source ../common.sh


KERNEL_TARGET=$HFLHOME/build/s2e/$LINUX

CONFIG=$(realpath $2)

function setup {
    mkdir -p $HFLHOME/build/s2e/

    pushd $HFLHOME/build/s2e/ >/dev/null
    tar xvfz $HFLHOME/$LINUX.tar.gz 
    popd
}

function build {
    pushd $KERNEL_TARGET >/dev/null
    make clean

    #### config
    if [ -z $CONFIG ]; then
        make defconfig
    else
        cp $CONFIG .config
    fi

    #C_INCLUDE_PATH=../include:$C_INCLUDE_PATH fakeroot -- make deb-pkg -j $(nproc) CC=$CC LOCALVERSION=-s2e V=1
    #fakeroot -- make deb-pkg -j $(nproc) CC=$CC LOCALVERSION=-s2e V=1
    make deb-pkg -j $(nproc) CC=$CC LOCALVERSION=-s2e V=1
    popd
}

setup
build

