#!/bin/bash -e

### leave out i386-softmmu from QEMU_TARGETS (Makefile) ... but not perfect

OPTION="install"

function setup {
    sudo apt-get install build-essential
    sudo apt-get install cmake
    sudo apt-get install wget
    sudo apt-get install git
    sudo apt-get install texinfo
    sudo apt-get install flex
    sudo apt-get install bison
    sudo apt-get install python-dev

    sudo apt-get install libdwarf-dev
    sudo apt-get install libelf-dev
    sudo apt-get install libboost-dev
    sudo apt-get install zlib1g-dev
    sudo apt-get install libjemalloc-dev
    sudo apt-get install nasm
    sudo apt-get install pkg-config
    sudo apt-get install libmemcached-dev
    sudo apt-get install libvdeplug-dev
    sudo apt-get install libpq-dev
    sudo apt-get install libc6-dev-i386
    sudo apt-get install libprocps4-dev         ### libprocps3-dev is also OK for Ubuntu 14.04 while libprocps-dev is for 18.04
    sudo apt-get install libboost-system-dev
    sudo apt-get install libboost-serialization-dev
    sudo apt-get install libboost-regex-dev
    sudo apt-get install libprotobuf-dev
    sudo apt-get install protobuf-compiler
    sudo apt-get install libbsd-dev
    sudo apt-get install libglib2.0-dev
    sudo apt-get install python-docutils

    # llvm-3.3 package is not available in Ubuntu 16.04
    # sudo apt-get build-dep llvm-3.3
    sudo apt-get build-dep qemu
}

setup

CURDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
S2E_DIR=$CURDIR/../../s2e

rm -rf $S2E_DIR/build >> /dev/null
mkdir -p $S2E_DIR/build
pushd $S2E_DIR/build
make V=1 -f ../Makefile $OPTION
popd

