#!/bin/bash -e

S2EDIR=..
VERBOSE="V=1"

pushd $S2EDIR/build
#rm -rf stamps/libs2e-*
rm -rf stamps/libs2e-release-make
rm -rf stamps/libs2e-release-install
#rm -rf stamps/z3-configure
#rm -rf stamps/z3-make
#rm -rf stamps/klee-*
#rm -rf stamps/klee-release-make
#rm -rf stamps/klee-release-install
#make V=1 -f ../../Makefile stamps/libs2e-release-install
#make $VERBOSE -f ../Makefile install-debug
make $VERBOSE -f ../Makefile install
popd

