#!/bin/bash -e


ARCH=x86_64


TMPDIR=$S2EGUESTIMG/.tmp-output/debian-9.2.1-x86_64/
OUTDIR=$S2EGUESTIMG/output/debian-9.2.1-x86_64/


#ISO=./cdrom/debian-9.3.0-amd64-netinst.iso

NOREBOOT="-no-reboot"
GRAPHIC="-nographic -monitor null"
ENABLEKVM="-enable-kvm"
NET="-net none"

MEM="-m 512M"   # enough for 4.14 base
#MEM="-m 1G"   # enough for 4.15 full device
#CDROM="-cdrom $ISO"

#CPU="-cpu core2duo"

QEMU=$S2EBUILD/opt/bin/qemu-system-$ARCH

MODE=release
