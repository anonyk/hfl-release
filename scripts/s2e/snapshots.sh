#!/bin/bash -e

source ../common.sh

DISK="-drive if=ide,index=0,file=$DISKDIR/$IMAGE.s2e,format=s2e,cache=writeback"

OPTION="-enable-serial-commands"   ## take snapshot and then automatic termination .. (s2e specific option??)
SERIAL="-serial file:./serial_ready.txt"

VM_NUM=2 #### EDIT

LIBS2E=$S2EBUILD/libs2e-release/x86_64-softmmu/libs2e.so


function run {
    cp ./samples/launch$1.sh launch.sh
    sudo virt-copy-in -a $DISKDIR/$IMAGE.s2e launch.sh /home/s2e/   
    rm launch.sh

    LD_PRELOAD=$LIBS2E $QEMU $DISK $MEM $ENABLEKVM $NET $OPTION 
}

### take snapshots !!
for n in $(seq "$VM_NUM"); do
    run $(expr $n - 1)
done
