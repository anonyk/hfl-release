#!/bin/bash

source ./common.sh


pushd $HFLHOME >/dev/null

echo "[*] Downloading Kernel $LINUX_VERSION ..."
#wget -O kernel.tar.gz $LINUX_URL 2> /dev/null
wget $LINUX_URL 2> /dev/null

popd
