#!/bin/bash

source ./common.sh
rm -rf ./workdir/* >/dev/null
rm vm*.smt2.log >/dev/null


CONFIG_FILE=sample.cfg      # EDIT

TMP_DIR=tmp


mkdir -p ./$TMP_DIR
rm -rf $TMP_DIR/*
cp ./sample/opcodes.h ./$TMP_DIR/
cp ./sample/s2e.h ./$TMP_DIR/


$SYZHOME/bin/syz-manager -config $CONFIG_FILE -enable enable_syscalls &

