#!/usr/bin/python

import os
import sys
import subprocess
import time
import multiprocessing
import shutil

ROOT = os.path.dirname(os.path.abspath(__file__))
HFLHOME = os.getenv("HFLHOME")
RUN_S2E = os.path.join(HFLHOME + "/scripts/s2e/run-libs2e-S2E.sh")

numOfVMs = 2    # EDIT

#### parallel
def post_processing_par(fname, idx):
    with open(workdir + "/" + vmlogdir + str(idx) + "/s2e-last/all-queries.smt2", 'r') as queries:
        content = ""
        for line in queries:
            if not line.strip() or line.startswith(";"):    # filter out all comments
                continue
            content = content + line

        if content.find("assert") >= 0:
            with open(fname + ".orig", "w") as fd:
                fd.write(content)
        else:
            with open(fname + ".orig.empty", "w") as fd:
                fd.write(content)


def thread_S2E(f):
    global idx

    fname = f[7:][:-4]
    fname = os.path.join(tmpdir, fname)
    print f, fname

    c_str = "./s2eget $FILE && chmod +x ./$FILE && sudo ./$FILE\n"
    c_str = "FILE=" + f + "\n\n" + c_str
    c_str = "#!/bin/bash -e\n\n" + c_str
    open(tmpdir + "/symbolic_script" + str(idx) + ".sh", "w").write(c_str)
    #with open(tmpdir + "/symbolic_script" + str(idx) + ".sh", "w") as fd:
    #    fd.write(c_str)
    print idx


    os.chdir(vmlogdir + str(idx))
    #cmds = [RUN_S2E + " " + str(idx) + " " + str(f[-9:][:-4])]  
    cmds = [RUN_S2E, str(idx), str(f[-9:][:-4])]  
    output = _execute(cmds) 
    os.chdir("..")
    print output

    post_processing_par(fname, idx)

    exe = os.path.join(tmpdir, f)
    os.rename(exe, exe + ".done")   

    return 


def execute_onS2Ev2_par():

    ### create s2e vm dir 
    for i in range(numOfVMs):
        if not os.path.exists(vmlogdir + str(i)):
            os.mkdir(vmlogdir + str(i))
            shutil.copy2("sample/library.lua", vmlogdir + str(i) + "/library.lua")

    manager = multiprocessing.Manager()
#    lock = manager.Lock()
    q = manager.Queue()
    for i in range(numOfVMs):
        q.put(i)

    pool = multiprocessing.Pool(numOfVMs, parallel_init, (q, ))

    nameList = []
    for f in os.listdir(tmpdir):
        if f.startswith("sym") and f.endswith(".exe"):
            nameList.append(f)

    if len(nameList) == 0:
        # print "There is no .exe"
        pool.close()
        return

    pool.map(thread_S2E, nameList)
#   pool.apply_async(thread_func, (f, name, lock))

    #pool.join()
    pool.close()
    return


def parallel_init(queue):
    global idx
    idx = queue.get()


#### sequential
def post_processing_seq(fname):
    with open(workdir + "/" + vmlogdir + str(1) + "/s2e-last/all-queries.smt2", 'r') as queries:
        content = ""
        for line in queries:
            if not line.strip() or line.startswith(";"):    # filter out all comments
                continue
            content = content + line

        if content.find("assert") >= 0:
            with open(fname + ".orig", "w") as fd:
                fd.write(content)
        else:
            with open(fname + ".orig.empty", "w") as fd:
                fd.write(content)


def execute_onS2Ev2_seq():

    ### create s2e vm dir 
    if not os.path.exists(vmlogdir + str(1)):
        os.mkdir(vmlogdir + str(1))
        shutil.copy2("sample/library.lua", vmlogdir + str(1) + "/library.lua")

    for f in os.listdir(tmpdir):
        if f.startswith("sym") and f.endswith(".exe"):
            fname = f[7:][:-4]
            fname = os.path.join(tmpdir, fname)
            print f, fname

            c_str = "./s2eget $FILE && chmod +x ./$FILE && sudo ./$FILE\n"
            c_str = "FILE=" + f + "\n\n" + c_str
            c_str = "#!/bin/bash -e\n\n" + c_str
            open(tmpdir + "/symbolic_script1.sh", "w").write(c_str)
            
            os.chdir(vmlogdir + str(1))
            cmds = [RUN_S2E, str(1), str(f[-9:][:-4])]
            output = _execute(cmds) 
            os.chdir('..')
            print output

            post_processing_seq(fname)

            exe = os.path.join(tmpdir, f)
            os.rename(exe, exe + ".done") 

    return


def _execute(cmds):
    try:
        output = subprocess.check_output(cmds, stderr=subprocess.STDOUT)
    except Exception, e:
        output = str(e.output)
    return output


def start():
    while True:
        execute_onS2Ev2_par() 
#        execute_onS2Ev2_seq()


if __name__ == "__main__":
    tmpdir = os.path.join(ROOT, "tmp")
    workdir = ROOT
    vmlogdir = "s2e-log.vm"
    if not os.path.exists(tmpdir):
        os.mkdir(tmpdir)

    start()

