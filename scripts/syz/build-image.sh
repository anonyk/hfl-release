#!/bin/bash -e

source ../common.sh


OS_DIR=$SYZHOME/tools/wheezy
TMP_MOUNT_DIR=/mnt/wheezy

DISK_TARGET=$HFLHOME/build/syz/disk

NEW_IMAGE_PATH=$DISK_TARGET/disk.img


#### prerequisite ... build wheezy.img
if [ ! -f $SYZHOME/tools/wheezy.img ]; then
    pushd $SYZHOME/tools
    ./create-image.sh
    popd
fi

mkdir -p $DISK_TARGET

dd if=/dev/zero of=$NEW_IMAGE_PATH bs=1G seek=7 count=1

#### disk image partition (util-linux-2.27 > .. ubuntu-16.04)
sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << FDISK_CMDS  | sudo fdisk $NEW_IMAGE_PATH
n       # add primary partition
p       #
        # partition number
        # default - first sector 
+5G     # partition size
n       # add extended partition
e       #
        # partition number
        # default - first sector 
        # partition size
n       # add logical partition
        # default - first sector 
+256M   # partition size
n       # add new partition
        # default - first sector 
+256M   # partition size
n       # add new partition
        # default - first sector 
+256M   # partition size
n       # add new partition
        # default - first sector 
+256M   # partition size
n       # add new partition
        # default - first sector 
+256M   # partition size
n       # add new partition
        # default - first sector 
+256M   # partition size
n       # add new partition
        # default - first sector 
        # default - last sector 
p
a       # set bootable
1
w       # write partition table and exit
FDISK_CMDS


#### bind disk image with loop device
LOOPBACK=$(losetup -f)
sudo losetup -P $LOOPBACK $NEW_IMAGE_PATH   ## automatically consider partition

#### format 
##/sbin/mkfs.ext4 -F $NEW_IMAGE_PATH
sudo mkfs.ext4 ${LOOPBACK}p1
sudo mkfs.ext2 ${LOOPBACK}p5
sudo mkfs.ext3 ${LOOPBACK}p6
sudo mkfs.xfs ${LOOPBACK}p7
sudo mkfs.btrfs ${LOOPBACK}p8
sudo mkfs.reiserfs ${LOOPBACK}p9
sudo mkfs.jfs ${LOOPBACK}p10
sudo mkfs.f2fs ${LOOPBACK}p11

#### copy root file system
sudo mkdir -p $TMP_MOUNT_DIR
#sudo mount -o loop $NEW_IMAGE_PATH $TMP_MOUNT_DIR
sudo mount -o loop ${LOOPBACK}p1 $TMP_MOUNT_DIR
sudo cp -a $OS_DIR/. $TMP_MOUNT_DIR/.
sudo mkdir -p $TMP_MOUNT_DIR/mnt/ext3
sudo mkdir -p $TMP_MOUNT_DIR/mnt/ext2
sudo mkdir -p $TMP_MOUNT_DIR/mnt/btrfs
sudo mkdir -p $TMP_MOUNT_DIR/mnt/xfs
sudo mkdir -p $TMP_MOUNT_DIR/mnt/reiserfs
sudo mkdir -p $TMP_MOUNT_DIR/mnt/jfs
sudo mkdir -p $TMP_MOUNT_DIR/mnt/f2fs
sudo sh -c "echo /dev/sda5 /mnt/ext2 ext2 defaults,noatime 0 0 >>$TMP_MOUNT_DIR/etc/fstab"
sudo sh -c "echo /dev/sda6 /mnt/ext3 ext3 defaults,noatime 0 0 >>$TMP_MOUNT_DIR/etc/fstab"
sudo sh -c "echo /dev/sda7 /mnt/xfs xfs defaults,noatime 0 0 >>$TMP_MOUNT_DIR/etc/fstab"
sudo sh -c "echo /dev/sda8 /mnt/btrfs btrfs defaults,noatime 0 0 >>$TMP_MOUNT_DIR/etc/fstab"
sudo sh -c "echo /dev/sda9 /mnt/reiserfs reiserfs defaults,noatime 0 0 >>$TMP_MOUNT_DIR/etc/fstab"
sudo sh -c "echo /dev/sda10 /mnt/jfs jfs defaults,noatime 0 0 >>$TMP_MOUNT_DIR/etc/fstab"
sudo sh -c "echo /dev/sda11 /mnt/f2fs f2fs defaults,noatime 0 0 >>$TMP_MOUNT_DIR/etc/fstab"
sudo umount $TMP_MOUNT_DIR

#### unbind
sudo losetup -d $LOOPBACK


