#!/bin/bash

source ../common.sh

pushd ../../src/github.com/google/syzkaller/
#make extract TARGETOS=linux TARGETARCH=amd64 SOURCEDIR=$KERNEL_TARGET   
make generate
make clean
make -j$(nproc) TARGETOS=linux TARGETARCH=amd64
popd
