#!/bin/bash -e

source ../common.sh

VMLINUX=$LINUX/vmlinux

OPT=-falex

KERNEL_TARGET=$HFLHOME/build/syz/$LINUX

CONFIG=$(realpath $2)

function setup {
    mkdir -p $HFLHOME/build/syz/

    pushd $HFLHOME/build/syz/ >/dev/null
    echo "[*] Unpacking Kernel $LINUX_VERSION ..."
    tar xvfz $HFLHOME/$LINUX.tar.gz 
    if [ -z target ]; then
        rm target
    fi
    ln -s $LINUX target
    popd
}

function build {
    pushd $KERNEL_TARGET >/dev/null
    make clean

    #### config
    if [ -z $CONFIG ]; then
        make defconfig
    else
        cp $CONFIG .config
    fi

    echo "[*] Building Kernel $LINUX_VERSION ..."
    make KCFLAGS="$OPT" CC=$CC $VERBOSE -j`nproc` 
    popd 
}

setup
build

