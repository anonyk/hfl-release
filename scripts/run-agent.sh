#!/bin/bash

source ./common.sh

## for z3 python
export S2EHOME=$S2EHOME
Z3_PY_PATH=$S2EHOME/build/z3-z3-4.6.0/build/python/
export PYTHONPATH=$Z3_PY_PATH
export Z3_LIBRARY_PATH=$Z3_PY_PATH
export LD_LIBRARY_PATH=$Z3_PY_PATH


function z3_py_build {
    pushd $S2EHOME/build/z3-z3-4.6.0/ >/dev/null
    python scripts/mk_make.py --staticlib
    cd build && make -j`nproc`
    cd ..
    popd >/dev/null
}

if [ ! -d "$Z3_PY_PATH" ]; then
    echo "[ERR] $Z3_PY_PATH not exist, need to build Z3..."
    z3_py_build
fi

./agent.py &

