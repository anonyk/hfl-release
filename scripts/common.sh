#!/bin/bash


CURDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
HFLHOME=$(realpath $CURDIR/../)
export HFLHOME=$HFLHOME

SYZHOME=$HFLHOME/src/github.com/google/syzkaller/
S2EHOME=$HFLHOME/s2e


S2EBUILD=$S2EHOME/build/
S2EGUESTIMG=$S2EHOME/guest-images/
S2EKERNEL=$S2EHOME/s2e-linux-kernel/
export S2EHOME=$S2EHOME

CC=$HFLHOME/gcc/newgcc/bin/gcc


LINUX_VERSION="4.15"     # EDIT
LINUX=linux-$LINUX_VERSION
LINUX_URL="https://cdn.kernel.org/pub/linux/kernel/v4.x/$LINUX.tar.gz"


# Download go from https://golang.org/dl/
export GOROOT=$HOME/go1.12  # EDIT
export PATH=$GOROOT/bin:$PATH
export GOPATH=$HFLHOME/



#### for Syz
KERNEL_TARGET=$CURDIR/../build/syz/target


#### for S2E
DISKDIR=$HFLHOME/build/s2e/disk
IMAGE=image.raw

NOREBOOT="-no-reboot"
GRAPHIC="-nographic -monitor null"
ENABLEKVM="-enable-kvm"
NET="-net none"

MEM="-m 512M"

QEMU=$S2EBUILD/opt/bin/qemu-system-x86_64

