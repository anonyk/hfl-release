
-------------------------------------------------------------------------------
-- This section configures the S2E engine.
s2e = {
    logging = {
        -- Possible values include "info", "warn", "debug", "none".
        -- See Logging.h in libs2ecore.
        console = "debug",
        logLevel = "debug",
    },

    -- All the cl::opt options defined in the engine can be tweaked here.
    -- This can be left empty most of the time.
    -- Most of the options can be found in S2EExecutor.cpp and Executor.cpp.
    kleeArgs = {
        "--max-solver-time=60",  
        "--verbose-fork-info",  
        "--verbose-state-switching",
        "--verbose-tb-finalize",
        "--fork-on-symbolic-address=false", 
        "--use-query-log=all:smt2,solver:smt2",
        "--use-end-query-pc-log=true",
        "--use-asm-addresses",
        "--verbose-on-symbolic-address", 
        "--print-forking-status",
        "--debug-constraints",
        "--debug-validate-solver"
    },
}

-- Declare empty plugin settings. They will be populated in the rest of
-- the configuration file.
plugins = {}
pluginsConfig = {}

-- Include various convenient functions
dofile('library.lua')

-------------------------------------------------------------------------------
-- This plugin contains the core custom instructions.
-- Some of these include s2e_make_symbolic, s2e_kill_state, etc.
-- You always want to have this plugin included.

add_plugin("BaseInstructions")

-------------------------------------------------------------------------------
-- This plugin implements "shared folders" between the host and the guest.
-- Use it in conjunction with s2eget and s2eput guest tools in order to
-- transfer files between the guest and the host.

add_plugin("HostFiles")
pluginsConfig.HostFiles = {
    baseDirs = {
        "/PATH/hfl/scripts/tmp/"
    },
    allowWrite = true,
}

