#!/usr/bin/python

import os
import sys
import subprocess
import glob
import time
import random
import shutil
import re
import multiprocessing
import StringIO
from z3 import *

SYZROOT = os.path.dirname(os.path.abspath(__file__))
PROG2C = os.path.join(SYZROOT, "../src/github.com/google/syzkaller/bin/syz-prog2c")
PROGTRANS = os.path.join(SYZROOT, "../src/github.com/google/syzkaller/bin/syz-progTrans")

BUF_SIZE = 30

prog_prefix = "sys_04_"
sym_prefix = "sym_04_"
prog_suffix = ".prog"
exe_suffix = ".exe"
c_suffix = ".c"
smt_suffix = ".smt2"
orig_suffix = ".orig"


##### disable symbolic syscalls #####
disabled_set = ["mmap", "accept", "bind", "socket", "syz_open_pts", "close", "openat", "shutdown", "inotify_init", "inotify_init1", "fsync", "sync", "syncfs", "fdatasync", "rmdir", "flock", "unlink", "rt_sigreturn", "pause", "sched_yield", "membarrier", "open", "mlockall", "munlockall", "unshare", "getgid", "getegid", "getpid", "gettid"]    

##### enable symbolic syscalls ######
all_set = ["ioctl", "prctl", "futex", "capset", "capget", "ptrace", "perf_event_open", "sysinfo", "setsockopt", "eventfd2", "uname", "getsockopt", "eventfd", "syslog", "lookup_dcookie", "write", "fallocate", "io_setup", "times", "sendto", "recvfrom", "pwrite64", "pread64", "read", "getdents", "getdents64"]    

## ioctl /dev/XX files edit
ioctl_dev = ["mouse", "mice", "usbmon", "sg", "ircomm", "usb", "rtc", "rfkill", "ppp", "irnet", "hwrng", "hpet", "pktcdvd", "cuse", "sequencer", "sequencer2", "lightnvm", "midi", "admmidi", "amidi", "dmmidi", "autofs", "mixer", "char", "block", "floppy", "parport", "pps", "tty0", "mem", "port", "fb0", "md", "watchdog", "uinput", "snapshot", "filesystem"]

## write /dev/XX files edit
write_dev = ["tty0", "mem", "port", "admmidi", "vcs", "vcsn", "vcsa", "cuse", "fb0", "floppy", "md", "vga_arbiter", "autofs", "userio", "watchdog", "parport", "ppp", "uinput", "mixer", "vhci", "rfkill", "hwrng", "snapshot", "kmsg", "kmem", "rdma", "filesystem"]



def get_value():

    visit = {}
    visit_fullname = {}

    ### upload and apply existing result to visit
    for f in os.listdir(tmpdir):
        if f.endswith(smt_suffix + ".val") :  
            with open(os.path.join(tmpdir, f), 'r') as lines:
                for line in lines:
                    print "before: " + line
                    line = '[' + line.split('[')[1] 
                    line = line.replace(", ", "], [")
                    line = line.replace("[else -> ", "")
                    line = line.replace(" ->", ",")
                    line = '[' + line

                    k = f[:-21] + line 
                    k = k.strip()
                    visit[k] = True
                    visit_fullname[k] = f[:-4]
                    print "after: "+ f[:-21] + line + " sizeofk:", len(k)

    while True:
        for f in os.listdir(tmpdir):
            if f.endswith(smt_suffix) :  
                print "[get_value] processing...", f
                smt = os.path.join(tmpdir, f)
#               try:
#                   os.remove(os.path.join(tmpdir, f) + ".val")
#               except OSError:
#                   pass

                s = Solver()
                s.set("timeout", 8000)
                #print parse_smt2_file(f)   # why not working???
                parsed = parse_smt2_string(open(os.path.join(tmpdir, f)).read())
                s.assert_exprs(parsed)  #s.add(parsed)

                res = s.check()
                if res == unsat:
                    core = s.unsat_core()
                    print "\tUNSAT.. ", len(core)
                    os.rename(smt, smt + ".UNSAT")
                    continue
                elif res == unknown:
                    print "\tUNKNOWN.. "
                    os.rename(smt, smt + ".UNKNOWN")
                    continue

                m = s.model()
                print "\tSAT!! : ", m


                #### filtering values
                buf = ""
                unique = False
                zeros = []

                #### each element of the result
                for d in m.decls():     
                    if str(d).find("__NR_") < 0:   
                        continue

                    decl = m[d]

                    #### filter invalid element e.g. 
                    if str(decl).find(" -> ") < 0:
                        print "\tdecl.as_list() is invalid (" + str(decl) +")"
                        continue

                    #### filter duplicate entry ?? Not really
                    k = f[:-17] + str(decl.as_list())
                    k = k.strip()
                    if k in visit.keys():
                        print "\tDUPLICATE SMT2: ", k , visit_fullname[k]
                    else:
                        print "\tUNIQUE SMT2: ", k , f, " sizeofk:", len(k)
                        visit[k] = True
                        visit_fullname[k] = f
                        unique = True

                    #### remove all zero element
                    nonzero = False     # initialize
                    for e in range(decl.num_entries()):
                        elem = decl.entry(e)
#                        print elem.arg_value(0)    # index
#                        print elem.num_args()
                        if str(elem.value()) != "0":
                            nonzero = True
                    if nonzero == True:
                        buf = buf + str(d) + " " + str(decl).replace("\n", "") + "\n"
                    else:   # zero ... [X -> 0, Y -> 0, ... K -> 0, else -> ?]
                        zeros.append(str(d) + " " + str(decl).replace("\n", "") + "\n")

                #### put zero entry if 
                for z in zeros:
                    if not re.findall(re.findall('_a._', z)[0], buf):
                        buf = buf + z

                if buf != "" and unique == True:

                    ### write to .val
                    open(smt + ".val", "w").write(buf)

                    ### mutate prog to .val
                    cmds = [PROGTRANS, "-prog", "tmp/" + prog_prefix + f[:-10] + "prog.done", "-smt", "tmp/" + f + ".val"]
                    output, error = _execute(cmds)

                os.rename(smt, smt + ".done")


def extract_constraints():
    while True:
        for f in os.listdir(tmpdir):
            if f.endswith(orig_suffix) :
                name = os.path.join(tmpdir, f[:-5])
                print "[extract_constraints] processing...", f
                smt2_str = ""
                flag = False
                smt2_count = 0
                with open(os.path.join(tmpdir, f), 'r') as lines:
                    for line in lines:
                        if smt2_count > 300:  # edit ... number of generation is limited to 300
                            print "[extract_constraints] Stopped due to More than 1000"
                            break
                        if line.find("(set-logic") >= 0:   ## first line of each query
                            flag = True
                            smt2_str = smt2_str + line 
                        elif line.find("(exit)") >= 0 and flag==True:   # last line
                            smt2_str = smt2_str + line
                            flag = False
                            if smt2_str != "":
                                name2 = name + "." + str(random.randint(10000,99999))
                                open(name2 + smt_suffix, "w").write(smt2_str)
                                smt2_str = ""

                                smt2_count += 1
                            continue
                        elif flag == True:
                            smt2_str = smt2_str + line

                    orig = os.path.join(tmpdir, f)
                    os.rename(orig, orig + ".done")
                    print "\tDONE!! ", f
    return 


def build():
    while True:
        for f in os.listdir(tmpdir):
            if f.startswith(sym_prefix) and f.endswith(c_suffix):
                sfile = os.path.join(tmpdir, f)
                exe = os.path.join(tmpdir, f[:-2] + exe_suffix)
                cmds = ["gcc", sfile, "-pthread", "-static", "-o", exe]
                print "[build] ", cmds
                output = _execute(cmds)
                print "\t",output

                os.rename(sfile, sfile + ".done")   # filename change ... X.c -> X.c.done
    return


### Transform .prog to .c
def gen_c_file():

    disabled = {} 
    for s in disabled_set :
        disabled[prog_prefix + s] = True

    enabled = {} 
    for s in all_set:  #edit
        enabled[prog_prefix + s] = True


    while True:
        for f in os.listdir(tmpdir):
            if not f.endswith(prog_suffix):
                continue

#            if f.split(".")[0].split("-")[0] in disabled:
#                os.remove(os.path.join(tmpdir, f))
#            elif f.split(".")[0].split("-")[0] in enabled:

            prog_file = os.path.join(tmpdir, f)
            c_file = os.path.join(tmpdir, f[:len(prog_suffix)*(-1)] + c_suffix)
#            callname = f[len(prog_prefix):].split(".")[0].replace("-", "$")


            cmds = [PROG2C, "-prog", prog_file, "-name", f[len(prog_prefix):].split(".")[0]]
            print "[gen_c_file] ", cmds
            c_str = _execute2(cmds)  

            c_str = "#include \"s2e.h\"\n\n" + c_str
            open(c_file, "w").write(c_str)

            os.rename(prog_file, prog_file + ".done")

    return 


### Make sysargs symbolic
def make_symbolic(ioctl_newdev, write_newdev):

    while True:
        for f in os.listdir(tmpdir):
            if f.startswith(prog_prefix) and f.endswith(c_suffix):
                print "[make_symbolic] ", f
                valid = False
                c_file = os.path.join(tmpdir, f)
                fsym = f.replace(prog_prefix, sym_prefix)
                s_file = os.path.join(tmpdir, fsym)
                stmts = ""

                if f.find("perf_event_open") >= 0:
                    stmts = stmts + "#include <linux/perf_event.h>\n"
                if f.find("sysinfo") >= 0:
                    stmts = stmts + "#include <linux/sysinfo.h>\n"
                if f.find("uname") >= 0:
                    stmts = stmts + "#include <linux/utsname.h>\n"
                if f.find("times") >= 0:
                    stmts = stmts + "#include <linux/times.h>\n"
                if f.find("sendmsg") >= 0:
                    stmts = stmts + "#include <sys/socket.h>\n"

                
                sys_name = f[len(prog_prefix):].split(".")[0]
                sys_name_full = sys_name.replace("-", "$")             # e.g., "ioctl$TIOCSETD"
                sys_name = "__NR_" + sys_name.split("-")[0]     # e.g., "__NR_ioctl", "__NR_ptrace"

                s_1 = "static uintptr_t execute_syscall(int nr, uintptr_t a0, uintptr_t a1, uintptr_t a2, uintptr_t a3, uintptr_t a4, uintptr_t a5, uintptr_t a6, uintptr_t a7, uintptr_t a8) {\n return syscall(nr, a0, a1, a2, a3, a4, a5);\n}\n"
                s0 = "static uintptr_t execute_syscall(int nr, uintptr_t a0, uintptr_t a1, uintptr_t a2, uintptr_t a3, uintptr_t a4, uintptr_t a5, uintptr_t a6, uintptr_t a7, uintptr_t a8) {\n uintptr_t ret;\n\t\ts2e_disable_forking();\n"
                s1 = "\t\tret = syscall(nr, a0, a1, a2, a3, a4, a5);\n"
                s2 = "\t\treturn ret;\n}\n\n"

                
                exec_func = ""

                ### 1. Insert a temp function (per-syscall) based on file name
                if sys_name == "__NR_ioctl" : 
                    #if sys_name_full.find("$") < 0 or sys_name_full.split("$")[1] in ioctl_newdev:
                        exec_func = exec_func + s0 + "\t\ts2e_make_concolic(&a1, sizeof(a1), \"" + sys_name + "_a1\");\n"
                        exec_func = exec_func + "\t\ts2e_make_concolic(a2, " + str(BUF_SIZE) + ", \"" + sys_name + "_a2\");\n" 
                        exec_func = exec_func + s1 + "\t\ts2e_print_expression(\"" + fsym + "\", a1);\n" + s2
#                    else:
#                        exec_func = exec_func + s_1
#                        valid = True

                elif sys_name == "__NR_write" :    # buf (in)

                    #if sys_name_full.find("$") < 0 or sys_name_full.split("$")[1] in write_newdev :
                        #exec_func = exec_func + s0 + "\t\ts2e_make_concolic(a1, a2, \"" + sys_name + "_a1\");\n"
                    exec_func = exec_func + s0 + "\t\ts2e_make_concolic(a1, " + str(BUF_SIZE) + ", \"" + sys_name + "_a1\");\n"
                    exec_func = exec_func + s1 + "\t\ts2e_print_expression(\"" + fsym + "\", a1);\n" + s2

#                    else:
#                        exec_func = exec_func + s_1
#                        valid = True

                elif sys_name == "__NR_sendto" :
                        #exec_func = exec_func + s0 + "\t\ts2e_make_concolic(a1, a2, \"" + sys_name + "_a1\");\n"
                        exec_func = exec_func + s0 + "\t\ts2e_make_concolic(a1, " + str(BUF_SIZE) + ", \"" + sys_name + "_a1\");\n"
                        exec_func = exec_func + s1 + "\t\ts2e_print_expression(\"" + fsym + "\", a1);\n" + s2

                elif sys_name == "__NR_read" :    # buf (out) -> (inout)
                    #if sys_name_full.find("$") < 0 or sys_name_full.endswith("$eventfd"):
                        #exec_func = exec_func + s0 + "\t\ts2e_make_concolic(a1, a2, \"" + sys_name + "_a1\");\n"
                        exec_func = exec_func + s0 + "\t\ts2e_make_concolic(a1, " + str(BUF_SIZE) + ", \"" + sys_name + "_a1\");\n"
                        exec_func = exec_func + s1 + "\t\ts2e_print_expression(\"" + fsym + "\", a1);\n" + s2
#                    else:
#                        exec_func = exec_func + s_1
#                        valid = True

                elif sys_name == "__NR_setsockopt" :    
                    exec_func = exec_func + s0 + "\t\ts2e_make_concolic(&a2, sizeof(a2), \"" + sys_name + "_a2\");\n"
                    #exec_func = exec_func + "\t\ts2e_make_concolic(a3, a4, \"" + sys_name + "_a3\");\n"
                    exec_func = exec_func + "\t\ts2e_make_concolic(a3, " + str(BUF_SIZE) + ", \"" + sys_name + "_a3\");\n"
                    exec_func = exec_func + s1 + "\t\ts2e_print_expression(\"" + fsym + "\", a3);\n" + s2

                elif sys_name == "__NR_sendmsg" :    
#                    exec_func = exec_func + s0 + "\t\ts2e_make_concolic(((struct msghdr *)a1)->msg_control, ((struct msghdr*)a1)->msg_controllen, \"" + sys_name + "_a1\");\n" #### optional control
                    exec_func = exec_func + s0 + "\t\ts2e_make_concolic(((struct msghdr *)a1)->msg_iov->iov_base, ((struct msghdr*)a1)->msg_iov->iov_len, \"" + sys_name + "_a1\");\n"
                    exec_func = exec_func + s1 + "\t\ts2e_print_expression(\"" + fsym + "\", a1);\n" + s2


                elif sys_name == "__NR_perf_event_open":   
                    exec_func = exec_func + s0 + "\t\ts2e_make_concolic((struct perf_event_attr *)a0, sizeof(struct perf_event_attr), \"" + sys_name + "_a0\");\n"
                    exec_func = exec_func + s1 + "\t\ts2e_print_expression(\"" + fsym + "\", a0);\n" + s2

                elif sys_name == "__NR_recvfrom"  :   
                        exec_func = exec_func + s0 + "\t\ts2e_make_concolic(a1, " + str(BUF_SIZE) + ", \"" + sys_name + "_a1\");\n"
                        exec_func = exec_func + s1 + "\t\ts2e_print_expression(\"" + fsym + "\", a1);\n" + s2

                elif sys_name == "__NR_syslog" or sys_name == "__NR_pwrite64" or sys_name == "__NR_pread64" or sys_name == "__NR_readlink" or sys_name == "__NR_lookup_dcookie" or sys_name == "__NR_getdents"  or sys_name == "__NR_getdents64" :   # done 
                        exec_func = exec_func + s0 + "\t\ts2e_make_concolic(a1, " + str(BUF_SIZE) + ", \"" + sys_name + "_a1\");\n"
                        #exec_func = exec_func + s0 + "\t\ts2e_make_concolic(a1, a2, \"" + sys_name + "_a1\");\n"
                        exec_func = exec_func + s1 + "\t\ts2e_print_expression(\"" + fsym + "\", a1);\n" + s2

                else:
                    exec_func = exec_func + s_1
                    valid = True


                ### 2. Scan the entire prog and find the marked stmt and make change 
                pattern = "syscall(" + sys_name 
                lines = open(c_file, 'r').readlines()
                lines2 = ""

                ## alignment (remove '\n')
                F = False
                for line in lines:
                    if F == True:
                        if line.find(");") >= 0:
                            F = False
                        else:
                            line = line.replace("\n", "")
                    elif line.find("ALEX037F") >= 0 and line.find(pattern) >= 0:
                        if line.find(");") < 0:
                            line = line.replace("\n", "")
                            F = True
                    lines2 = lines2 + line

                ## transform
                for line in StringIO.StringIO(lines2):
                    if line.find("void loop()") >= 0:   # definition of execute_syscall
                        stmts = stmts + exec_func + line
                    elif line.find("ALEX037F") >= 0 and line.find(pattern) >= 0:
                        args = line.replace("syscall", "execute_syscall").split(",")
                        if (10 > len(args)):
                            l = args[len(args)-1].split(")")
                            for f in range(10 - len(args)):
                                l[0] = l[0] + ", 0" 
                            args[len(args)-1] = ")".join(l)
                        stmts = stmts + ",".join(args) 
                    else:
                        stmts = stmts + line


                if valid == False:
                    open(s_file, "w").write(stmts)
                    os.rename(c_file, c_file + ".done")
                else:
                    os.rename(c_file, c_file + ".invalid")
    return


def _execute(cmds):
    error = False
    try:
        output = subprocess.check_output(cmds, stderr=subprocess.STDOUT)
    except Exception, e:
        output = str(e.output)
        error = True
    return output, error


def _execute2(cmds):    # potential clang-format error ... sol) install clang-format-3.8  ??
    try:
        output = subprocess.check_output(cmds, stderr=None)
    except Exception, e:
        output = str(e.output)
    return output


if __name__ == "__main__":
    tmpdir = os.path.join(SYZROOT, "tmp")
    if not os.path.exists(tmpdir):
        os.mkdir(tmpdir)

#    nocopy = False
#    if len(sys.argv) == 2 and sys.argv[1] == "-nocopy" :
#        nocopy = True

    ioctl_newdev = {}
    for s in ioctl_dev :
        ioctl_newdev[s] = True


    write_newdev = {}
    for s in write_dev :
        write_newdev[s] = True


    multiprocessing.Process(name="gen_c_file", target=gen_c_file).start()
    multiprocessing.Process(name="make_symbolic", target=make_symbolic, args=(ioctl_newdev, write_newdev,)).start()
    multiprocessing.Process(name="build", target=build).start()
    multiprocessing.Process(name="extract_constraints", target=extract_constraints).start()
    multiprocessing.Process(name="get_value", target=get_value).start()

#    if not nocopy : 
#        multiprocessing.Process(name="copy_to_singleVM", target=copy_to_singleVM).start()


